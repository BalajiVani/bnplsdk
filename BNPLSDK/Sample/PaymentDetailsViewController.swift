//
//  PaymentDetailsViewController.swift
//  Sample
//
//  Created by Balaji  on 24/02/22.


//

import UIKit
import CoreMedia

struct PaymentModel{
    var section:String?
    var row:[PurchaseModel]
}

struct PurchaseModel{
    var name:String?
    var value:String?
}



class PaymentDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!

    private var cellTypesArray: Array<UITableViewCell> = []
    
    private var paymentDetailsModel = [PaymentModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        localizedUpdate()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    @IBAction func backActn(_ sender: UIButton) {
        self.dismissVC()
    }
    
    func loadData(){
        self.cellTypesArray = [CreditDetailsTableViewCell(),PurchaseDetailsTableViewCell(),CreditDetailsTableViewCell()]
    
        self.paymentDetailsModel = [PaymentModel(section: "Purchase Details (in INR)", row: [
            PurchaseModel(name: "Purchase Details (in INR)", value: ""),
            PurchaseModel(name: "Price", value: "12,000"),
            PurchaseModel(name: "Interest (% pa if any)", value: "0.00"),
            PurchaseModel(name: "Discount", value: "0.00"),
            PurchaseModel(name: "EMI Plan", value: "4000 for 3 months"),
            PurchaseModel(name: "Total Amount Payable", value: "12,000"),
            ])]
        self.tableView.reloadData()
    }
}

extension PaymentDetailsViewController {
    private func registerCells(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName:"CreditDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "CreditDetailsTableViewCell")
        tableView.register(UINib(nibName:"PurchaseDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "PurchaseDetailsTableViewCell")
        tableView.register(UINib(nibName:"FooterTableViewCell", bundle: nil), forCellReuseIdentifier: "FooterTableViewCell")
    }
    
    private func localizedUpdate(){
        
    }
}

extension PaymentDetailsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return paymentDetailsModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentDetailsModel[section].row.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreditDetailsTableViewCell") as! CreditDetailsTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseDetailsTableViewCell") as! PurchaseDetailsTableViewCell
        let data = paymentDetailsModel[indexPath.section].row[indexPath.row]
        if indexPath.row == 0{
            cell.breakUpDescLbl.text = "Purchase Details (in INR)"
            cell.mainView.backgroundColor = .setColour(colour: .systemGray)
        }else if indexPath.row == paymentDetailsModel[indexPath.section].row.count-1{
            cell.breakUpDescLbl.text = data.name ?? EMPTY
            cell.breakupValueLbl.text = data.value ?? EMPTY
            cell.breakUpDescLbl.font = .setCustomFont(name: .bold, size: .x14)
            cell.breakupValueLbl.font = .setCustomFont(name: .bold, size: .x14)
        }else{
            cell.breakUpDescLbl.text = data.name ?? EMPTY
            cell.breakupValueLbl.text = data.value ?? EMPTY
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FooterTableViewCell") as! FooterTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 250
    }
}
