//
//  UIViewController-Extension.swift
//  BNPLSDK
//
//  Created by Balaji  on 21/02/22.
//

import UIKit

extension UIViewController {
    
    /*Dismiss or Pop back*/
    func dismissVC(){
        if (self.presentingViewController != nil){
            self.dismiss(animated: true, completion: nil)
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func alertWithNoAction(title : String = ERROR, delay : Int = 2, completion: @escaping ((_ success :Bool) -> Void)){
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        DispatchQueue.main.async {
            self.present(alertController, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(delay)){
                    alertController.dismiss(animated: true, completion: nil)
                    completion(true)
                }
            }
        }
    }
    
    func delay(interval: TimeInterval, closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
            closure()
        }
    }
    
    func alertWithNoAction(title : String = "Coming Soon ...", delay : Int = 2){
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        self.present(alertController, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(delay)){
                alertController.dismiss(animated: true, completion: nil)
            }
        }
    }
}

/* Open web view Viewcontroller*/
extension UIViewController {
    func openWebViewVC(url:String,title:String){
        let viewController = UIStoryboard(name: "BNPL", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        if url.isEmpty{
            self.alertWithNoAction(title: "NO Data")
        }
        else
        {
            viewController.vcTitle = title
            viewController.getURl = url
            self.present(viewController, animated: true, completion: nil)
        }
        
    }
}

/** Extension to show Generic Message Alert Controller throughout the App **/
extension UIViewController{
    enum AlertTitle: String{
        case Success = "Success"
        case Error = "Error"
        case Alert = "Alert"
    }
    
    func showMessageAlert(title: String = AlertTitle.Error.rawValue, message: String?, showRetry: Bool = true, retryTitle: String? = nil, showCancel: Bool = true, cancelTitle: String? = nil, onRetry: (() -> ())?, onCancel: (() -> ())?){
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            if showRetry{
                alertController.addAction(UIAlertAction(title: retryTitle ?? "Retry", style: .default, handler: { (retry) in
                    guard let onRetry = onRetry else{
                        return
                    }
                    onRetry()
                }))
            }
            if showCancel{
                alertController.addAction(UIAlertAction(title: cancelTitle ?? "Cancel", style: .cancel, handler: { (cancel) in
                    guard let onCancel = onCancel else{
                        return
                    }
                    onCancel()
                }))
            }
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func loader() -> UIAlertController {
        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 10, width: 35, height: 35))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating()
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        return alert
    }
    
    func stopLoader(loader : UIAlertController) {
        DispatchQueue.main.async {
            loader.dismiss(animated: true, completion: nil)
        }
    }
}

/* UIApplication Extension */
extension UIApplication {

    /*Get the Top VC*/
    class func getTopViewController(base: UIViewController? = UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

