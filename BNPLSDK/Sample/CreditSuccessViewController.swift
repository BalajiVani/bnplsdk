//
//  CreditSuccessViewController.swift
//  BNPLSDK
//
//  Created by Balaji  on 24/02/22.
//

import UIKit

class CreditSuccessViewController: UIViewController {
    
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var creditStatusLbl:UILabel!
    @IBOutlet weak var verifyBtn:UIButton!
    @IBOutlet weak var creditView:UIView!
    public var creditStatus : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyles()
        self.loadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.headerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.headerView.backgroundColor = .setColour(colour: .systemGray)
    }
    
    /* Set Style */
    func setStyles(){
        self.verifyBtn.primaryBtn()
    }
    
    func loadData(){
        self.creditStatusLbl.text = creditStatus ? "Sorry, Credit limit unavilable!" :  "We’re happy to provide you credit line!"
        self.verifyBtn.setTitle( creditStatus ? "Select another payment method" : "Continue to Purchase", for: .normal)
        self.creditView.isHidden = creditStatus
    }
    
    @IBAction func onContinue(_ sender:UIButton){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PaymentDetailsViewController") as! PaymentDetailsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func backActn(_ sender: UIButton) {
        self.dismissVC()
    }
    
}
