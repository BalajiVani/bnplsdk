//
//  ViewController.swift
//  Sample
//
//  Created by Balaji  on 21/02/22.
//

import UIKit
import BNPLSDK

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func bnplClicked(_ sender: Any) {
        BNPLCD.shared.present(from: self)
        BNPLCD.shared.onClick = { status in
            print("status:\(status)")
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CreditSuccessViewController") as! CreditSuccessViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }

}

