//
//  FooterTableViewCell.swift
//  Sample
//
//  Created by Balaji  on 24/02/22.
//

import UIKit

class FooterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var continueBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.continueBtn.primaryBtn()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
