//
//  PurchaseDetailsTableViewCell.swift
//  Sample
//
//  Created by Balaji  on 24/02/22.
//

import UIKit

class PurchaseDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var breakUpDescLbl:UILabel!
    @IBOutlet weak var breakupValueLbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
