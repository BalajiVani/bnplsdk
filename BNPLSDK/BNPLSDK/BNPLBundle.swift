//
//  BNPLBundle.swift
//  BNPLSDK
//
//  Created by Balaji  on 21/02/22.
//

import Foundation
import UIKit

/*To Get bundle Name*/
class BNPLBundle {
    static var shared = BNPLBundle()
    private init() { }
    
    /*Access from Resource Bundle*/
    var currentBundle: Bundle  {
        let frameworkBundle = Bundle(identifier: "com.bnpl.sdk")!
        return frameworkBundle
    }
    
    /*To get DeviceId and BundleId*/
    var deviceId: String? { return UIDevice.current.identifierForVendor?.uuidString.filter { $0 != "-" } }
    var bundleId: String? { return Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String }
}

