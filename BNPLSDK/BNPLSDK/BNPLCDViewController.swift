//
//  BNPLCDViewController.swift
//  BNPLSDK
//
//  Created by Balaji  on 21/02/22.
//

import UIKit
import Photos

class BNPLCDViewController: UIViewController {
    
    @IBOutlet weak var acceptLbl:UILabel!
    @IBOutlet weak var profileView:UIView!
    @IBOutlet weak var profileBtn:UIButton!
    @IBOutlet weak var continueBtn:UIButton!
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var mobileTxtField:UITextField!
    private var imagePicker: ImagePicker!
    private var maxLength = 10
    
    /*Present Controller*/
    internal class func controller() -> BNPLCDViewController {
        return UIStoryboard(name: "BNPL", bundle: BNPLBundle.shared.currentBundle).instantiateViewController(withIdentifier: "BNPLCDViewController") as! BNPLCDViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        self.setStyles()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.headerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.headerView.backgroundColor = .setColour(colour: .systemGray)
    }
    
    /*load data*/
    func loadData(){
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.linkableText()
    }
    
    @IBAction func profileActn(_ sender:UIButton){
        self.imagePicker.present(from: sender)
    }
    
    /* Set Style */
    func setStyles(){
        self.mobileTxtField.delegate = self
        self.continueBtn.primaryBtn()
    }
    
    func linkableText(){
        acceptLbl.text = "By clicking on ‘continue’ below, I agree to the  Privacy Policy, T&C and Equifax Credit Information"
        let text = (acceptLbl.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        
        underlineAttriString.apply(color: UIColor.setColour(colour: Colour.skyBlue), subString: "Privacy Policy")
        underlineAttriString.apply(color: UIColor.setColour(colour: Colour.skyBlue), subString: "T&C" )
        underlineAttriString.apply(color: UIColor.setColour(colour: Colour.skyBlue), subString: "Equifax Credit Information" )
        
        acceptLbl.attributedText = underlineAttriString
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        acceptLbl.isUserInteractionEnabled = true
        acceptLbl.addGestureRecognizer(tapAction)
    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        if gesture.didTapAttributedTextInLabel(label: acceptLbl, targetText: "T&C") {
            self.termsAndConditiions()
        } else if gesture.didTapAttributedTextInLabel(label: acceptLbl, targetText: "Privacy Policy") {
            self.privacyPolicy()
        } else {
            self.EquifaxCreditInformation()
        }
    }
    
    func termsAndConditiions(){
        self.openWebViewVC(url: "https://www.google.co.in/", title: "T&C")
    }
    
    func privacyPolicy(){
        self.openWebViewVC(url: "https://www.google.co.in/", title: "Privacy Policy")
    }
    
    func EquifaxCreditInformation(){
        self.openWebViewVC(url: "https://www.google.co.in/", title: "Equifax Credit Information")
    }
    
    func validation(){
         
    }
    
    @IBAction func onContinue(_ sender:UIButton){
        let viewController = self.storyboard?.instantiateViewController(identifier: "VerificationViewController") as! VerificationViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func handleButtonViewVisibility(textField: UITextField?) {
        if let text = textField?.text, !text.isEmpty{
            continueBtn.alpha = 1.0
            continueBtn.isEnabled = true
        }else{
            continueBtn.alpha = 0.5
            continueBtn.isEnabled = false
        }
    }
}

/*TextField Delegate*/
extension BNPLCDViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.mobileTxtField {
            return range.location < self.maxLength
        }
        return true
    }
}


extension NSMutableAttributedString {
    
    func apply(color: UIColor, subString: String) {
        if let range = self.string.range(of: subString) {
            self.apply(color: color, onRange: NSRange(range, in:self.string))
        }
    }
    
    // Apply color on given range
    func apply(color: UIColor, onRange: NSRange) {
        self.addAttributes([NSAttributedString.Key.foregroundColor: color],
                           range: onRange)
    }
}

extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, targetText: String) -> Bool {
        guard let attributedString = label.attributedText, let lblText = label.text else { return false }
        let targetRange = (lblText as NSString).range(of: targetText)
        //IMPORTANT label correct font for NSTextStorage needed
        let mutableAttribString = NSMutableAttributedString(attributedString: attributedString)
        mutableAttribString.addAttributes(
            [NSAttributedString.Key.font: label.font ?? UIFont.smallSystemFontSize],
            range: NSRange(location: 0, length: attributedString.length)
        )
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: mutableAttribString)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
                                                        locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}


/* Image Picker delegate */
extension BNPLCDViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if let img = image {
            profileBtn.setBackgroundImage(image, for: .normal)
            if let pngRepresentation = img.pngData() {
                UserDefaults.standard.set(pngRepresentation, forKey: UserDefaultsKeys.profileImage.rawValue)
                UserDefaults.standard.synchronize()
            }
        }
    }
}
