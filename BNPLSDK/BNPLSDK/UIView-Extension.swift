//
//  UIView-Extension.swift
//  BNPLSDK
//
//  Created by Balaji  on 21/02/22.
//

import Foundation
import UIKit

extension UIButton {
    
    /*Set Primary Color*/
    func primaryBtn(shadow :Bool = false){
        self.adjustsImageWhenHighlighted = false
        self.contentEdgeInsets = UIEdgeInsets(top:0,left:10,bottom:0,right:10)
        self.backgroundColor = .setColour(colour: .primaryColor)
        self.layer.cornerRadius = 5
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        //self.decideTextDirection()
        self.titleLabel?.font = .setCustomFont(name: .regular, size: .x18)
        self.setTitleColor(.white, for: .normal)
        if shadow {
            self.layer.shadowOffset = CGSize(width: 0, height: 5)
            self.layer.shadowColor = UIColor.setColour(colour: .lightGrey).cgColor
            self.layer.shadowOpacity = 1
            self.layer.shadowRadius = 5
        }
    }
}

extension String {
    
    /*Check Empty String*/
    var isEmptyStr: Bool{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
    }
}

extension UIView {
    /*Set Corner for selected Edge*/
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    /*Set Circle*/
    func circleRadius(){
        self.layer.cornerRadius = self.frame.size.height/2
    }
}
