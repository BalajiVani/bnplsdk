//
//  BNPL.swift
//  BNPLSDK
//
//  Created by Balaji  on 21/02/22.
//

import UIKit
import Foundation

/*Shared Protocal*/

public class BNPLCD: BNPLCDProtocol {
   
    /*SharedInstance*/
    public static var shared: BNPLCDProtocol = BNPLCD()
    
    /*Internal Shared Instance*/
    internal static var sharedInternal: BNPLCD = {
        return shared as! BNPLCD }()
    
    public init() { }
    
    /*Local variable to store request payload*/
    public var BNPLCDParams: [String : Any]?
    
    /*Check for Biometric*/
    let bioMetricAuth = BiometricIDAuth()
    
    /*Provision to enable Biometric option from Client*/
    public var  BNPLEnableBioMetric: Bool?
    
    /*Set Theme Color HexString*/
    public var themeColorHexString: String?
    
    public var onClick: ((String) -> Void)?
    
    /*Hanlde parent and present viewcontrollers*/
    private var presentedViewController: UIViewController? = nil
    private var parentController: UIViewController? = nil
    
    /*Present BNPL Card detail View*/
    public func present(from parentController: UIViewController) {
        BNPLCD.shared.BNPLEnableBioMetric = BNPLCD.shared.BNPLEnableBioMetric ?? true
        DispatchQueue.main.async {
            self.parentController = parentController
            parentController.modalPresentationStyle = .overFullScreen
            if (self.presentedViewController != nil) {
                self.presentedViewController?.dismiss(animated: true, completion: {
                    DispatchQueue.main.async {
                        self.presentCurrentViewController(parentController: parentController)
                    }
                })
            } else {
                self.presentCurrentViewController(parentController: parentController)
            }
        }
    }
    
    func presentCurrentViewController(parentController: UIViewController) {
        if UIDevice.current.isJailBroken{
            self.parentController?.alertWithNoAction(title: "Widget cannot be invoke in this device",completion: { _ in
                //TO DO
            })
        }else{
            /*Check for Biometric Enablement*/
            if BNPLCD.shared.BNPLEnableBioMetric ?? false {
                /*Biometric Authentication*/
                bioMetricAuth.authenticateUser() { [weak self] message in
                    if let messageString = message {
                        self?.parentController?.alertWithNoAction(title: messageString, completion: { _ in
                            //TO DO
                        })
                    } else {
                            DispatchQueue.main.async() {
                            //Success -> Present the BNPL Card View
                            print("Bio Auth Success....")
                            if let viewController = self?.viewControllerToPresent() {
                                self?.presentedViewController = viewController
                                self?.parentController?.present(viewController, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }else{
                /*Present BNPL Card View Controller*/
                let viewController = self.viewControllerToPresent()
                self.presentedViewController = viewController
                parentController.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    /*Dismiss the Presented BNPL Card Page from client App*/
    public func dismiss() {
        DispatchQueue.main.async {
            self.presentedViewController?.dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    self.presentedViewController = nil
                    self.parentController = nil
                }
            })
        }
    }
    
    /*BNPL View Controller to Present*/
    func viewControllerToPresent() -> UIViewController {
        let rootViewController = BNPLCDViewController.controller()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.modalPresentationStyle = .fullScreen
        return navigationController
    }
}

