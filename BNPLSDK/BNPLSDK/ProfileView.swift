//
//  ProfileView.swift
//  FinTechProductBase
//
//  Created by Balaji  on 15/12/20.
//  Copyright © 2020 sidhudevarayan. All rights reserved.
//

import UIKit

@IBDesignable
class ProfileView: UIView {
    
    @IBOutlet weak var view: UIView!{
        didSet{
        }
    }
    
    @IBOutlet weak var nameLbl: UILabel!{
        didSet{
            nameLbl.backgroundColor = .clear
            nameLbl.font = .setCustomFont(name: .medium, size: .x16)
            nameLbl.textColor = UIColor.setColour(colour: Colour.black)
        }
    }
    @IBOutlet weak var profBtn: UIButton!{
        didSet{
            profBtn.backgroundColor = .clear
        }
    }
    
    var onClick:(()->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        self.view.circleRadius()
        
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor = UIColor.setColour(colour: Colour.white).cgColor
        
        addSubview(view)
        loadProfileImage()
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ProfileView", bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    @IBAction func profileActn(_ sender: UIButton) {
        self.onClick?()
    }
    
    func loadProfileImage(showBlueBackground: Bool = false){
        nameLbl.text = ""
        profBtn.setBackgroundImage(UIImage(named: "m2p"), for: .normal)

        if let imageData = UserDefaults.standard.object(forKey: UserDefaultsKeys.profileImage.rawValue) as? Data,
           let image = UIImage(data: imageData) {
            profBtn.setBackgroundImage(image, for: .normal)
        }
    }
}
