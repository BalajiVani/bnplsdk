//
//  BNPL_Protocols.swift
//  BNPLSDK
//
//  Created by Balaji  on 21/02/22.
//

import Foundation
import UIKit
import LocalAuthentication

public protocol BNPLCDProtocol {
    
    /*Shared Instance for Client*/
    static var shared: BNPLCDProtocol { get }
    
    /*Payload from client*/
    var BNPLCDParams: [String: Any]? { get set }
    
    /*Provision to enable Biometric option from Client*/
    var BNPLEnableBioMetric: Bool? { get set }
    
    /*Present BNPL Card detail View*/
    func present(from parentController: UIViewController)

    /*Dismiss the Presented BNPL Card Page from client App*/
    func dismiss()
    
    /*Set Theme Color HexString*/
    var themeColorHexString: String? { get set }
    
    var onClick : ((String)->Void)? { get set }
    
}


/*Extension of Local Auth*/

extension LAContext {
    
    /*Enum for BioMetric Access*/
    enum M2PBiometricType {
        case none
        case touchID
        case faceID
        case error
    }
    
    /*Get the biometric type*/
    var m2p_BiometricType: M2PBiometricType {
        get {
            let context = LAContext()
            var error: NSError?
            guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
                print(error?.localizedDescription ?? "")
                if error?.code == -8{
                    return .error
                }
                return .none
            }
            if #available(iOS 11.0, *) {
                switch context.biometryType {
                case .none:
                    return .none
                case .touchID:
                    return .touchID
                case .faceID:
                    return .faceID
                @unknown default:
                    return .none
                }
            } else {
                return  .none
            }
        }
    }
}


/*Extension of UIView*/
extension UIView {
    /*Dismiss the custom view*/
    func dismissView(onCompletion completion : (()->Void)?){
        self.removeFromSuperview()
        completion?()
    }
}


