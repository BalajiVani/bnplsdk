//
//  VerificationViewController.swift
//  BNPLSDK
//
//  Created by Balaji  on 22/02/22.
//

import UIKit

class VerificationViewController: UIViewController {
    
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var expireLblTimer:UILabel!
    @IBOutlet weak var resendBtn:UIButton!{
        didSet{
            resendBtn.alpha = 0.5
            resendBtn.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var profileBtn:UIButton!
    @IBOutlet weak var verifyBtn:UIButton!
    @IBOutlet weak var segmentControl:UISegmentedControl!{
        didSet{
            if #available(iOS 13.0, *) {
                segmentControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
                segmentControl.tintColor = .setColour(colour: .primaryColor)
            } else {
                segmentControl.tintColor = .setColour(colour: .primaryColor)
            }
        }
    }
    @IBOutlet weak var verifyCodeTF:UITextField!
    @IBOutlet weak var otpTF:UITextField!
    @IBOutlet weak var otpMobileNoLbl:UILabel!
    @IBOutlet weak var errorMesgLbl:UILabel!
    
    private var imagePicker: ImagePicker!
    private var countdownTimer: Timer!
    private var totalTime = 120
    private var maxLength_OTP = 6
    private var maxLength_CARD = 4
    private var maxLength_PAN = 4
    
    public var getMobileNo : String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyles()
        self.startTimer()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.headerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.headerView.backgroundColor = .setColour(colour: .systemGray)
    }
    
    /* Set Style */
    func setStyles(){
        self.verifyCodeTF.delegate = self
        self.otpTF.delegate = self
        self.verifyBtn.primaryBtn()
    }
    
    func loadData(){
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        otpMobileNoLbl.text = "OTP has been sent to {+91 9840 263 236} \(getMobileNo)"
    }
    
    @IBAction func profileActn(_ sender:UIButton){
        self.imagePicker.present(from: sender)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        verifyCodeTF.placeholder = sender.selectedSegmentIndex == 1 ? "Last 4 digits of card" : "Last 4 digits of PAN No"
    }
    
    @IBAction func onContinue(_ sender:UIButton){
        BNPLCD.shared.dismiss()
        BNPLCD.shared.onClick?("CreditSucess")
    }
    
    @IBAction func backActn(_ sender: UIButton) {
        self.dismissVC()
    }
    
}

/*TextField Delegate*/
extension VerificationViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.verifyCodeTF{
            return range.location < self.maxLength_CARD
        }else if textField == self.otpTF{
            return range.location < self.maxLength_OTP
        }
        return true
    }
}

/*OTP Coundown Timer*/
extension VerificationViewController {
    
    func startTimer() {
        totalTime = 20
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        expireLblTimer.text = "in \(timeFormatted(totalTime)) seconds"
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        resendBtn.alpha = 1
        resendBtn.isUserInteractionEnabled = true
        
        guard countdownTimer != nil else {
            return
        }
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        //let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d", seconds)
    }
    
    @IBAction func `resendActn`(_ sender:UIButton){
        countdownTimer.invalidate()
        resendBtn.alpha = 0.5
        resendBtn.isUserInteractionEnabled = false
    }
}

/* Image Picker delegate */
extension VerificationViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if let img = image {
            profileBtn.setBackgroundImage(image, for: .normal)
            if let pngRepresentation = img.pngData() {
                UserDefaults.standard.set(pngRepresentation, forKey: UserDefaultsKeys.profileImage.rawValue)
                UserDefaults.standard.synchronize()
            }
        }
    }
}
